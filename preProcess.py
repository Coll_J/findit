import pandas as pd
import json

df = pd.read_csv('hotel_bookings.csv')
df.drop('reservation_status_date', axis=1, inplace=True)

nan_replacements = {"children": 0.0,"country": "Unknown", "agent": 0, "company": 0}

df.fillna(nan_replacements, inplace=True)

# ------------------------------------------------
mapping = json.load(open('map.txt'))
df = df.replace(mapping)
mapping = json.load(open('col_dict.txt'))
df = df.astype(mapping)
print(df.dtypes)
# df = df.apply(pd.to_numeric)
df.to_csv('clean_data.csv')        
print(df.head())