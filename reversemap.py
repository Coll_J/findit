import json

val = json.load(open('map.txt'))
col = json.load(open('col_dict.txt'))

val_map = {}
col_map = {}
for name, value in val.items():
    print(f"name: {name} val: {value}")
    val_map.update({value:name})

json.dump(val_map, open('map_rev.txt', 'w'))

for name, value in col.items():
    print(f"name: {name} val: {value}")
    col_map.update({value:name})
    
json.dump(col_map, open('col_rev.txt', 'w'))
