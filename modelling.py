import pandas as pd
import pickle

# pre-process
from sklearn.model_selection import train_test_split, KFold, cross_validate, cross_val_score
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.preprocessing import StandardScaler

# ml
from sklearn.svm import LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import BaggingClassifier

from sklearn.ensemble import VotingClassifier

df = pd.read_csv('clean_data.csv')

features = ["lead_time", "previous_cancellations", "days_in_waiting_list", "market_segment", "deposit_type", "customer_type"]

X = df[features]
print(X.head())
print("testis")
Y = df["is_canceled"]
# Y = Y.to_numpy()

# pre-process
# transformer = SimpleImputer(strategy="constant")
# preprocessor = ColumnTransformer(transformers=[("feat", transformer, features)])
scaler = StandardScaler()
X = scaler.fit_transform(X)
print(X)

split = KFold(n_splits=7, shuffle=True, random_state=50) 
base_models = [
    ("SVC", LinearSVC(random_state=50, C=1.0, max_iter=11000)),
    ("KNeighbor", KNeighborsClassifier(n_neighbors=3)),
    ("DTree", DecisionTreeClassifier(random_state=50))
]

# print(base_models[2])
# name, model = base_models[2]
# print(model)

for name, model in base_models:
    print(f"processing {name}...")
    # model_steps = Pipeline(steps=[('preprocessor', preprocessor), ('model', model)])
    result = cross_val_score(model, X, Y, cv=split)
    print(f"{name} CV score: {result.mean()}")
    filename = f"{name}.sav"
    pickle.dump(model, open(filename, "wb"))
    print("model saved")

# bagging
name, dtree_mod = base_models[2]
bag_model = BaggingClassifier(base_estimator=dtree_mod, n_estimators=100, random_state=40)
bag_result = cross_val_score(bag_model, X, Y, cv=split)
print(f"Bagging score: {bag_result.mean()}")
pickle.dump(model, open('bagging.sav', "wb"))

# voting
base_models.append(("Bagging", bag_model))
vote = VotingClassifier(base_models)
vote_result = cross_val_score(vote, X, Y, cv=split)
print(f"Vote score: {vote_result.mean()}")
pickle.dump(model, open('voting.sav', "wb"))
