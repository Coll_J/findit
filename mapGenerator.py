import pandas as pd
import numpy as np
import json
from pandas.api.types import is_string_dtype
from pandas.api.types import is_numeric_dtype

df = pd.read_csv('hotel_bookings.csv', index_col=0)
df.drop('reservation_status_date', axis=1, inplace=True)

print(is_string_dtype(df['country']))
mapping = {}

# if not (df['arrival_date_month'][1] in mapping):
#     mapping.update({df['arrival_date_month'][1]: 1})
#     print(mapping)
# else:
#     print("no")

print(len(df))
for (colName, colData) in df.iteritems():
    if is_string_dtype(df[colName]):
        mapping.update({colName: 'float64'})
        count = 1
        islisted = 0
        for i in range(len(df)):
            if not (df[colName][i] in mapping):
                islisted = 0
                mapping.update({df[colName][i]: count})
                print(mapping)
                count+=1
            else:
                if islisted > 30000:
                    break
                islisted+=1
            print(i)
        # print(mapping)
        # break
json.dump(mapping, open('map.txt', 'w'))