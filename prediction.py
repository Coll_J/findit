import pickle
import pandas as pd

from sklearn.svm import LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import BaggingClassifier

from sklearn.ensemble import VotingClassifier
from sklearn.preprocessing import StandardScaler

df = pd.read_csv('clean_data.csv')
df_real = pd.read_csv('hotel_bookings.csv')
df.drop(['is_canceled'], axis=1)
df_train = df.sample(frac = .8)

features = ["lead_time", "previous_cancellations", "days_in_waiting_list", "market_segment", "deposit_type", "customer_type"]
X_train = df_train[features]
Y_train = df_train["is_canceled"]
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)

X = df[features]
Y = df["is_canceled"]

model = pickle.load(open('first/DTree.sav', 'rb'))
model.fit(X, Y)
pred = model.predict(X)
res = pd.DataFrame(pred)
df_real['is_canceled_result'] = res

df_real.to_excel('final.xlsx', engine='xlsxwriter')
df_real.to_csv('backup.csv')
print(pred)
print(df_real.head())